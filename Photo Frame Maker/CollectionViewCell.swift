//
//  CollectionViewCell.swift
//  Photo Frame Maker
//
//  Created by OranjeTech on 01/09/2022.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
}
