//
//  ViewController.swift
//  Photo Frame Maker
//
//  Created by OranjeTech on 01/09/2022.
//

import UIKit
import Lottie
class ViewController: UIViewController {

    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    var timer = Timer()
    @IBOutlet weak var crownView: UIButton!
    var counter = 0
    let fonts = ["AAhlanWasahlan","AAntaraDistance","AAntiCorona","ABasterRules","ABlackCrown","Adistro","AgenGiveaway","AgreementSignature","AhayHore","Aidilfitri","AllertaStencil","AlmendraDisplay","Ambrosia","AmbyarSobat","AmneSans","AngkanyaSebelas","Anjhay","AnnyeongHaseyo","Anterobot","ApiNyala","ArianaVioleta","AspireDemibold","AstroSpace","Atmospheric","AttackGraffiti","Audiowide","AvoidLight","AwalRamadhan","BarbequeGrill","Baumans","BeckettRegular","Bignoodletitling","BlocpartyOutlineRegular","BlocpartyRegular","BlokStensilRegular","Bonida","BreeSerif","BroadwayRegular","BukhariscriptAlternates","BuyanBold","Catwalzhari","Chopinscript","CocoRegular","Chopsic","CollegiateheavyoutlineMedium","ComicShark","CraftingLesson","DaddyRewind","Dimasala","Do4BrainBold","Do4BrainLight","Edition","EvilEmpire","Excluded","Excludededitalic","Facon","FakeSerif","Fanzine","FasterOne","Freedom","GalacticBasic","Gecade","GecadeBold","GecadeBoldItalic","Gladius","GochiHnad","GoldenSentry","GunmetalRegular","Hacked","Handlee","HarryP","HeiReinaRegular","HellowinterHellowinter","Hokjesgeest","HumbelCafe","IrishuncialfabetaBold","Javans","JlrClonmacnoise","Joker","JosefinSansThin","Kantumruy","KashieMercy","KirvyBold","Knewave","LeckerliOne","LibreBarcode39Text","LibreBarcode128Text","LightmoonPersonalUse","MarianeLusia","MasPendiWow","Merienda","Monoton","MontserratSubrayada","Moonhouse","MrDafoe","NewWaltDisneyFontRegular","NewYearsParty","Niconne","Nokora","Nosifer","OldeEnglishRegular","Ontel","PeapodThai","PeapodThaiOt","Playball","PoiretOne","ProcrastinatingPixie","Quango","RandomXBold","Raphtalia","RaspberryCake","Rehat","Rembank","Righteous","RoguelandSlabBold","RuthCalligraph","SafetyThirdRegular","Sail","Sancreek","SatellaRugular","Satisfy","ShadowsIntoLight"]
    let sliderArray = ["slider","slider1","slider2","slider3"]
    let x = 50
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        pageControl.numberOfPages = sliderArray.count
        pageControl.currentPage = 0
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(slide), userInfo: nil, repeats: true)
        showCrownAnimation(view: crownView)
    }
    override func viewDidLayoutSubviews() {
        self.collectionview.layer.cornerRadius = 12
        self.collectionview.clipsToBounds = true
    }
    
    @objc func slide()
    {
        let index = IndexPath(row: counter, section: 0)
        if counter < sliderArray.count
        {
            pageControl.currentPage = counter
            collectionview.scrollToItem(at: index, at:.centeredHorizontally, animated: true)
            counter += 1
        }else{
            counter = 0
            pageControl.currentPage = 0
            collectionview.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
            counter = 1
        }
    }
    
    
    func showCrownAnimation(view:UIButton)
    {
        let animationView = AnimationView()
        animationView.animation = Animation.named("crown") //JSON file name
       // animationView.center = view.center
        animationView.frame = view.bounds
        animationView.backgroundColor = .clear
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .repeat(4)
        animationView.play()
        view.addSubview(animationView)
    }

}


extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
      
        cell.img.image = UIImage(named: sliderArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
   
    
   
    
    
}
